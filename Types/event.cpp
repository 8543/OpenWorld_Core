#include "event.h"

namespace OpenWorld
{
namespace Types
{
void Event::connect(sol::function func)
{
    m_connections.push_back(func);
}

void Event::register_class(sol::state_view& state)
{
    state.new_usertype<Event>("Event", "connect", &Event::connect);
}
}
}
