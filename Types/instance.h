#ifndef INSTANCE_H
#define INSTANCE_H

#include "event.h"

#include <sol.hpp>
#include <vector>

namespace OpenWorld
{
namespace Types
{
struct Instance {
   private:
    // MEMBERS

    // HEIRACHY
    std::vector<Instance*> m_children;
    Instance* m_parent = nullptr;
    std::string m_name;
    //
    // Helpers
    void removeFromParent();

   public:
    virtual void step(){ }
    virtual void destroy();
    // EVENTS
    Event m_childAdded;
    Event m_childRemoved;
    Event m_parentChanged;

    // Constructors
    Instance(std::string name);
    Instance(std::string name, Instance* parent);
    //
    // TODO getDescendants
    sol::table getChildren(sol::this_state self);
    std::vector<Instance*>& c_getChildren();

    Instance* findFirstChild(std::string name, bool recursive);

    // Property m_parent
    Instance* getParent();
    void setParent(Instance* rhs);

    // Property m_name
    std::string getName();
    void setName(std::string name);

    static void register_class(sol::state_view& state);

    // meta
    bool equal(Instance* other);
    Instance* index(std::string name);
};

// Bind
#define TYPE_INSTANCE_BIND(T) \
    "Parent", sol::property(&T::getParent, &T::setParent),                                   \
    "Name", sol::property(&T::getName, &T::setName),                                         \
    "getChildren", &T::getChildren,                                                          \
    "findFirstChild", &T::findFirstChild,                                                    \
    "ChildAdded", &T::m_childAdded,                                                          \
    "ChildRemoved", &T::m_childRemoved,                                                      \
    "ParentChanged", &T::m_parentChanged,                                                    \
    "destroy", &T::destroy,                                                                  \
    sol::meta_function::equal_to, &T::equal, sol::meta_function::index, &T::index



}
}

#endif
