#include "instance.h"

namespace OpenWorld
{
namespace Types
{

// ctor
Instance::Instance(std::string name)
        : m_name(name) {}
Instance::Instance(std::string name, Instance* parent)
        : Instance(name)
{
    setParent(parent);
}

// Destroy
void Instance::destroy() {
    // Remove from parent, destroy children, disconnect events
    // TODO disconnect events
    removeFromParent();
    for(auto &i : m_children) {
        i->destroy();
    }
}

void Instance::register_class(sol::state_view& state)
{
    state.new_usertype<Instance>(
            "Instance", sol::constructors<sol::types<std::string>, sol::types<std::string, Instance*>>(),
            TYPE_INSTANCE_BIND(Instance)
        );
}

//---

Instance* Instance::findFirstChild(std::string name, bool recursive)
{
    if(m_children.empty()) {
        return nullptr;
    }

    for(auto const& i : m_children) {
        if(i->m_name == name) {
            return i;
        }

        if(recursive) {
            Instance* ret = i->findFirstChild(name, true);
            if(ret != nullptr) {
                return ret;
            }
        }
    }

    return nullptr;
}

sol::table Instance::getChildren(sol::this_state self)
{
    sol::state_view state(self.L);
    sol::table ret = state.create_table(self.L, m_children.size(), 0);

    int i = 1;
    for(auto& c : m_children) {
        ret.set(i, c);
        i++;
    }

    return ret;
}

std::vector<Instance*>& Instance::c_getChildren() {
    return m_children;
}

// Parent
Instance* Instance::getParent()
{
    return m_parent;
}

void Instance::removeFromParent() {
    // Remove from current parent
    if(m_parent != nullptr) {
        for(auto i = m_parent->m_children.begin(); i != m_parent->m_children.end(); i++) {
            if(*i == this) {
                m_parent->m_children.erase(i);
                break;
            }
        }

        m_parent->m_childRemoved.call(this);
    }
}
void Instance::setParent(Instance* rhs)
{
    removeFromParent();

    m_parent = rhs;
    m_parentChanged.call(m_parent);

    if(m_parent != nullptr) {
        m_parent->m_children.push_back(this);
        m_parent->m_childAdded.call(this);
    }
}

// Name
std::string Instance::getName()
{
    return m_name;
}

void Instance::setName(std::string name)
{
    m_name = name;
}

bool Instance::equal(Instance* rhs)
{
    return rhs == this;
}

Instance* Instance::index(std::string name)
{
    return findFirstChild(name, false);
}
}
}
