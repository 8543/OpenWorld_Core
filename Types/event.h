#ifndef EVENT_H
#define EVENT_H

#include <iostream>
#include <sol.hpp>
#include <vector>

namespace OpenWorld
{
namespace Types
{
class Event
{
   public:
    std::vector<sol::function> m_connections;
    
    bool conntected() {
        return !(m_connections.empty());
    }

    void connect(sol::function func);

    template <typename... Args> void call(Args&&... args)
    {
        if(m_connections.empty()) {
            return;
        }
        for(auto i = m_connections.begin(); i != m_connections.end(); ) {
            const auto &m = *i;
            if(!m.valid()) {
                i = m_connections.erase(i);
                continue;
            }
            bool ret = m.call(std::forward<Args>(args)...);
            if(!ret) {
                i = m_connections.erase(i);
            } else {
                i++;
            }
        }
//        for(const auto& i : m_connections) {
//            bool ret = i.call(std::forward<Args>(args)...);
//        }
    }

    static void register_class(sol::state_view& state);
};
}
}

#endif
