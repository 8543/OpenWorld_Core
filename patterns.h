#ifndef CORE_PATTERNS_H
#define CORE_PATTERNS_H

#include <string>
#include <sstream>
#include <map>
#include <memory>

namespace OpenWorld { namespace System { namespace DesignPatterns {

/*
 * See owWindow/openworld_window.cpp Window::setupBullet
 * to see how this class is used
 */

template <typename T>
class TypeErasure {
public:
    static std::map<std::string, std::shared_ptr<void>> state;

    template <typename M>
    static M* getState(std::string key) {
        if(state.find(key) == state.end()) {
            return nullptr;
        }
        return static_cast<M*>( state[key].get() );
    }

    /*
     * setState("m_Str", std::string("hey");
     *
     * copies then allocates
     */
    template <typename M>
    static auto setState(std::string key, M&& val) {
        state[key] = std::make_shared<M>( std::move(val) );
        return static_cast<M*>( state[key].get() );
    }

    /*
     * setStatePtr("m_SceneMgr", m_SceneMgr);
     *
     * where m_SceneMgr is an already allocated pointer
     */
    template <typename M>
    static auto setStatePtr(std::string key, M* val) {
        state[key] = std::shared_ptr<M>( std::move(val) );
        return static_cast<M*>( state[key].get() );
    }

    /*
     * allocateStatePtr<btDbvtBroadphase>("m_Broadphase");
     *
     * btDbvtBroadphase must be created with "new btDbvtBroadphase"
     */
    template <typename M>
    static auto allocateStatePtr(std::string key) {
        state[key] = std::shared_ptr<M>(new M());
        return static_cast<M*>( state[key].get() );
    }

    /*
     * For a class which must be heap allocated, but also
     * requires constructor arguments
     *
     * allocateNew<Foo>("m_Test", 1,2,3);
     */

    template <typename M, typename ...Args>
    static auto allocateNew(std::string key, Args&&... args) {
        state[key] = std::shared_ptr<M>( new M( std::forward<Args>(args)... ));
        return static_cast<M*>( state[key].get() );
    };
};
template <typename T>
std::map<std::string, std::shared_ptr<void>> TypeErasure<T>::state;

/*
 * Example:
 *
 * class World : public Singleton<T> {
 *
 * }
 */
template <typename T>
class Singleton {
protected:
    Singleton() {}
    // Enforce one object
    Singleton(const Singleton&) {}
    Singleton& operator=(const Singleton&) {}
    virtual ~Singleton() {}
    //
    static T* m_Singleton;

public:
    static T* getSingletonPtr() {
        if(m_Singleton == nullptr) {
            m_Singleton = new T();
        }

        return m_Singleton;
    }

    static T& getSingleton() {
        T* ret_ptr = getSingletonPtr();

        return *ret_ptr;
    }
};
template <typename T>
T* Singleton<T>::m_Singleton = nullptr;

}}}

#endif //CORE_PATTERNS_H
