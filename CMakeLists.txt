cmake_minimum_required(VERSION 3.5)
project(Core)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -fPIC -shared")

set(SOURCE_FILES
        openworld_core.cpp
        Types/event.cpp
        Types/instance.cpp
        singletons.h)

include_directories(~/ClionProjects/)
add_library(Core SHARED ${SOURCE_FILES})