#ifndef OPENWORLD_TEST
#define OPENWORLD_TEST

// Header only
// Helps with testing

#include <sol.hpp>
#include "bind_type.h"
#include "openworld_core.h"

namespace OpenWorld
{
namespace Core
{
template <typename... Ts> class TestRunner : public Detail::StateEval
{
   public:
    TestRunner()
    {
        lua.open_libraries();
        // OpenWorld types
        sol::state_view state(lua.lua_state());
        ::OpenWorld::Types::Instance::register_class(state);
        ::OpenWorld::Types::Event::register_class(state);
        ::OpenWorld::Types::bindTs<Ts...>(state);
    }
};
}
}

#endif
