#ifndef OPENWORLD_CORE
#define OPENWORLD_CORE

#include <iostream>
#include <chrono>
#include <lua.hpp>
#include <vector>

#include <sol.hpp>

// For singleton
#include "patterns.h"
// For World members
#include <map>
#include <memory>

namespace OpenWorld
{

/*
 * World::getSingleton().state["m_Str"] = std::make_shared<std::string>(std::string("hey"));
 */

class World
        :   public System::DesignPatterns::Singleton<World>     ,
            public System::DesignPatterns::TypeErasure<World>   {
private:
    World() {}
public:

    /*
     * Need to use type erasure to use the same data across libraries,
     * not sure if there's a better solution.
     */
};

namespace Core
{

/*
 *  Thread should be run with a class from runner.h, not manually.
 *
 * ThreadRunner runner("test.lua");
 * runner.run<Types::test>();
 *
 * Second line will add extra types ontop of the basic Instance+event
 */
class Thread
{
private:
    using pair_t = std::pair<int, lua_State*>;
    std::vector<pair_t> m_threads;

    using bindfunc_t = std::function<void(sol::state_view&)>;
    bindfunc_t m_bindFunc;
    
    lua_State* m_EntryState;

    // Judging time
    std::chrono::steady_clock::time_point m_BeginTime;

    /*
      \brief Creates a new thread
      \param L Lua state
      \param n_args number of arguments to call the entry function with

      Creates a new thread, executes the entry function with the specified
      arguments and pushes it to m_threads.
      Stack should look like:

        -1: arg1
        -2: arg2
        -3: arg3
        -4: entry function

      new_thread(L, 3)
    */

    void new_thread(lua_State* L, int n_args = 0);

    float tick();

    /*
      \brief Enforces regular sleeping on a thread
      \param L Lua state

      By default, calling lua_resume will run until the next yield. This
      could potentially be a while, and it's a pain to manually manage.
      Used after thread creation \see new_thread
    */

    static void enforce_sleep(lua_State* L);
    static void release_sleep(lua_State* L);

    /*
      \brief Throws a Lua error
      \param L Lua state
      \param err_msg Error message to display

      Pushes err_msg to stack and calls lua_error
    */
    static void error(lua_State* L, const std::string& err_msg);

    /*
      \brief Binds types and functions
      \param L Lua state

      Binds the various functions and classes usable in Lua
    */
    void bind_environment(lua_State* L);

public:
    //--- Lua functions ---//
    int spawn(lua_State* L);

    /*
      \brief Starts a thread
      \param L Lua state
      \param n_args Number of arguments initially

      If args are being passed, it should be in the format:

        -1: arg1
        -2: arg2
        -3: arg3
        -4: entry func

      Thread(L,3);
    */
    Thread(lua_State* L, bindfunc_t bind_func, int n_args = 0);
    
    void step();
};
}
}

#endif
