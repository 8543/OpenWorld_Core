#ifndef CORE_SINGLETONS_H
#define CORE_SINGLETONS_H

#include "Types/event.h"
#include "patterns.h"

namespace OpenWorld {

/*
 * Singletons are bound in runner.h - ThreadRunner::run
 */
class InputSingleton
        : public System::DesignPatterns::Singleton<InputSingleton> {

    using Event = Types::Event;
public:

    Event evt_KeyPress;
    Event evt_KeyRelease;

    /*
     * Expects global value 'Singletons' to be a table
     */
    static void register_class(sol::state_view& state) {
        state.new_usertype<InputSingleton>(
                "InputSingleton",
                "KeyPressed", &InputSingleton::evt_KeyPress,
                "KeyReleased", &InputSingleton::evt_KeyRelease,
                sol::call_constructor, sol::no_constructor
        );
        sol::table singletons = state["Singletons"];
        singletons["Input"] = InputSingleton::getSingletonPtr();
    }
};

}

#endif //CORE_SINGLETONS_H
