#include <chrono>
#include "openworld_core.h"
#include "runner.h"

// Type includes in openworld_core.h

namespace OpenWorld
{
namespace Core
{
// Private / static

void Thread::new_thread(lua_State* L, int n_args)
{
    lua_State* ptr = lua_newthread(L);
    int indx = luaL_ref(L, LUA_REGISTRYINDEX);
    m_threads.push_back(pair_t(indx, ptr));

    // move function ref to new thread
    lua_xmove(L, ptr, n_args + 1);
    enforce_sleep(ptr);

    // resume once, eat LUA_OK
    int ret = lua_resume(ptr, NULL, n_args);

    lua_pop(L, n_args);

    if(ret != LUA_YIELD) {
        if(ret == LUA_OK) {
            // Only took one cycle
            m_threads.pop_back();
            return;
        }

        // error
        std::cout << "Lua Error: " << lua_tostring(ptr, -1) << "\n";
        m_threads.pop_back();
    }
}

void Thread::enforce_sleep(lua_State* L)
{
    auto hookFun = [](lua_State* lua, lua_Debug*) {
        if(lua_isyieldable(lua)) {
            lua_yield(lua, 0);
        }
    };

    lua_sethook(L, hookFun, LUA_MASKCOUNT, 10);
}

void Thread::release_sleep(lua_State* L) {
    auto hookFun = [](lua_State*, lua_Debug*) {};

    lua_sethook(L, hookFun, LUA_MASKCOUNT, 10);
}

void Thread::error(lua_State* L, const std::string& err_msg)
{
    lua_pushstring(L, err_msg.c_str());
    lua_error(L);
}

//------
Thread::Thread(lua_State* L, bindfunc_t bind_func, int n_args)
        : m_bindFunc(bind_func), m_BeginTime(std::chrono::steady_clock::now())
{
    if(!lua_isfunction(L, -1)) {
        throw std::runtime_error("Thread requires function on top of thread");
    }
    
    m_EntryState = L;
    bind_environment(L);

    // enforce sleep
    enforce_sleep(L);

    // Init parent state
    new_thread(L, n_args);

    // TODO Why is this needed
    m_threads.reserve(1000);
}

void Thread::step() {
    lua_State* L = m_EntryState;
    
    for(auto i = m_threads.begin(); i != m_threads.end();) {
        auto it = *i;
        int res = lua_resume(it.second, L, 0);

        if(res == LUA_YIELD) {
            i++;
        } else if(res == LUA_OK) {
            // thread gone
            i = m_threads.erase(i);
            break;
        } else {
            // error
            std::cout << "Lua Error: " << lua_tostring(it.second, -1) << "\n";
            i = m_threads.erase(i);
            break;
        }
    }
}

//--- Static Lua functions ---//

void Thread::bind_environment(lua_State* L)
{
    //--- Built in ---//
    sol::state_view state(L);
    state["spawn"] = [&](sol::this_state L) -> int { return spawn(L.L); };
    // Get time
    state["tickT"] = [&]() -> float { return tick(); };
    // Run code in one step
    state["__inline"] = [&](sol::protected_function func) {
        release_sleep(func.lua_state());
        func();
        enforce_sleep(func.lua_state());
    };

    // In built types
    Types::Instance::register_class(state);
    Types::Event::register_class(state);

    m_bindFunc(state);
}

float Thread::tick() {
    std::uint64_t ms =
    std::chrono::duration_cast<std::chrono::milliseconds>(
            (std::chrono::steady_clock::now() - m_BeginTime)
    ).count();

    return float(ms);
}

int Thread::spawn(lua_State* L)
{
    int n_arg = lua_gettop(L);

    // Error checking
    if(m_threads.size() >= 999) {
        error(L, "Can't exceed 1001 tasks per thread at once");
    }

    if(!lua_isfunction(L, -n_arg)) {
        error(L, "Spawn requires a function argument");
    }

    new_thread(L, n_arg - 1);
    return 0;
}


}
}
