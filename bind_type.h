#ifndef BIND_TYPE_H
#define BIND_TYPE_H

#include <sol.hpp>

namespace OpenWorld
{
namespace Types
{
template <typename... Ts> static void bindTs(sol::state_view& state)
{
    (void)std::initializer_list<int>{(Ts::register_class(state), 0)...};
}
}
}

#endif