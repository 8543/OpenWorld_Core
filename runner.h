#include "bind_type.h"
#include "openworld_core.h"
#include "Types/instance.h"

#include <sstream>
#include "singletons.h"

namespace OpenWorld
{
namespace Core
{
namespace Detail
{
class StateEval
{

   public:
    sol::state lua;

    template <typename R> R eval(std::string line)
    {
        std::stringstream ss;
        ss << "_eval = (" << line << ")";
        lua.script(ss.str());

        R ret = lua.get<R>("_eval");
        lua.script("_eval = nil");
        return ret;
    }

    void eval(std::string line)
    {
        lua.script(line.c_str());
    }
};
}

class ThreadRunner : public Detail::StateEval
{
   private:
    Thread* t;
    std::function<void(sol::state_view&)> m_loadFunc;
    ::OpenWorld::Types::Instance* m_RootPtr;

   public:
    ThreadRunner(std::string file, ::OpenWorld::Types::Instance* root_ptr)
    : m_RootPtr(root_ptr)
    {
        t = nullptr;
        lua.open_libraries();
        luaL_loadfile(lua, file.c_str());
    }

    // Needs to be out here, can't put on constructor
    template <typename... Ts> void run()
    {
        m_loadFunc = [&](sol::state_view& state) {

             // bind root instance
            state.set("root", m_RootPtr);
            // Make a Singleton table
            state.script("Singletons = {}");

            ::OpenWorld::Types::bindTs<Ts...>(state);
        };

        t = new Thread(lua, m_loadFunc, 0);
    }
    
    void step() {
        t->step();
    }

    ~ThreadRunner()
    {
        delete t;
    }
};
}
}
